<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResetPasswordsTable extends Migration
{
    public function up()
    {
        Schema::create('reset_passwords', function (Blueprint $table) {
                $table->id()->autoIncrement();
                $table->bigInteger('user_id')->nullable();
                $table->string('email');
                $table->string('token', 250);
                $table->timestamps();
            });
    }

    public function down()
    {
        Schema::dropIfExists('reset_passwords');
    }
}
