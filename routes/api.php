<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthenticationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('/users', [AuthenticationController::class, 'create'])->name('create');
Route::post('/login', [AuthenticationController::class, 'login'])->name('login');
Route::post('/reset', [AuthenticationController::class, 'resetPassword'])->name('reset-password');
Route::post('/newpassword', [AuthenticationController::class, 'newPassword'])->name('new-password');

Route::middleware('auth:api')->group(function () {
    Route::put('/users/{user}', [AuthenticationController::class, 'update'])->name('update');
    Route::get('/users', [AuthenticationController::class, 'showUsers'])->name('show');
    Route::get('/users/{user}', [AuthenticationController::class, 'showUser'])->name('show-user');
    Route::delete('/users/{user}', [AuthenticationController::class, 'delete'])->name('delete-user');
});




