<?php

namespace Tests\Unit;

use App\Mail\PasswordSent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;


    public function testResetEmail()
    {
        Mail::fake();
        $email = $this->faker->email;
        $token = \Str::random(20);
        Mail::to($email)->send(new PasswordSent($token));
        Mail::assertSent(PasswordSent::class);
    }


}
