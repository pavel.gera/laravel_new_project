<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;


class GetUsersServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->userService = app()->make(UserService::class);
    }

    /** @test */
    public function showUsers()
    {
        User::factory(5)->create();

        $response = $this->userService->getUsers();
        $this->assertCount(5, $response);
        $this->assertIsArray($response);
        foreach ($response as $email) {
            $this->assertIsString($email);
        }
    }
}

