<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteUserServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();
        $this->userService = app()->make(UserService::class);
    }

    /** @test */
    public function deleteUser()
    {
        $user = User::factory()->create();
        $this->userService->deleteUser($user);
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'status' => 2   //Inactive
        ]);
    }
}
