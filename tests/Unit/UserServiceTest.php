<?php

namespace Tests\Unit;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    private $userService;

    public function setUp(): void
    {
        parent::setUp();
        $this->userService = app()->make(UserService::class);
    }

    public function tests_create_method()
    {
        $email = $this->faker->email;
        $data = ['name' => $this->faker->name, 'email' => $email, 'password' => $this->faker->password];
        $createdUser = $this->userService->createUser($data);
        $this->assertInstanceOf(User::class, $createdUser);
        $this->assertDatabaseHas('users', ['email' => $email]);
    }
}
