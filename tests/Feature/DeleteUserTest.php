<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function deleteUser()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->actingAs($user)->delete(route('delete-user', $user));
        $response->assertSuccessful();
        $response->assertStatus(200);
        $response->assertJsonStructure(null, 200);
    }
}
