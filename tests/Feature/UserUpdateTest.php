<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class UserUpdateTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function user_can_update_own_profile()
    {
        $name = $this->faker->name;
        $headers = ['content-Type' => 'application/json', 'accept' => 'application/json'];
        $user = User::factory()->create();
        $user = Passport::actingAs($user);

        $response = $this->actingAs($user)->put(route('update', $user), ['name' => $name], $headers);
        $response->assertSuccessful();
        $response->assertStatus(200);
        $response->assertJsonStructure(null, 200);
    }
}
