<?php

namespace Tests\Feature;


use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ShowUserTest extends TestCase
{
    use RefreshDatabase, WithFaker;


    /** @test */
    public function showUser()
    {
        $user = User::factory()->create();
        Passport::actingAs($user);
        $response = $this->actingAs($user)->get(route('show-user', $user));
        $response->assertJson([
            'userInfo' => [
                'name' => true,
                'email' => true,
                'created_at' => true,
                'updated_at' => true
            ],
        ]);
    }
}
