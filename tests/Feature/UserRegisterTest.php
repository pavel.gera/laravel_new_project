<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function testRegister()
    {
        $password = $this->faker->password;
        $data = ['name' => $this->faker->name, 'email' => $this->faker->email, 'password' => $password, 'password_confirmation' => $password];
        $headers = ['content-Type' => 'application/json', 'accept' => 'application/json'];
        $response = $this->json('POST', 'api/users', $data, $headers)->assertStatus(201);
        $response->assertJsonStructure([
            'token' => 'token'
        ]);
    }
}
