<?php

namespace Tests\Feature;

use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginUserTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->userService = app()->make(UserService::class);
    }

    /** @test */
    public function loginUser()
    {
        $email = $this->faker->email;
        $password = $this->faker->password;
        $name = $this->faker->name;
        $data = ['name' => $name, 'email' => $email, 'password' => $password, 'password_confirmation' => $password];
        $headers = ['content-Type' => 'application/json', 'accept' => 'application/json'];
        $this->userService->createUser($data);

        $response = $this->json('POST', 'api/login', $data, $headers)->assertStatus(201);
        $response->assertJsonStructure([
            'token' => 'token'
        ]);
    }
}
