<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteUsersRequest;
use App\Http\Requests\GetUsersRequest;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\ValidateResetDataRequest;
use App\Models\ResetPassword;
use App\Models\User;
use App\Services\UpdatePasswordService;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\PasswordSent;
use Barryvdh\DomPDF\Facade as PDF;


class AuthenticationController extends Controller
{
    public function __construct(
        protected UserService           $userService,
        protected UpdatePasswordService $updatePasswordService
    ) {}

    // Create new user
    public function create(StoreUserRequest $storeUserRequest)
    {
        $user = $this->userService->createUser($storeUserRequest->validated());
        $token = $user->createToken('API Token')->accessToken;
        return response()->json(['token' => $token], 201);
    }

    // Login authenticated user
    public function login(LoginUserRequest $loginUserRequest)
    {
        $data = $loginUserRequest->validated();
        if (!auth()->attempt($data)) {
            return response(['error_message' => 'Incorrect Details.
            Please try again']);
        }

        $token = auth()->user()->createToken('API Token')->accessToken;
        return response()->json(['token' => $token], 201);
    }

    // Reset password
    public function resetPassword(ResetPasswordRequest $request)
    {
        $email = $request->input('email');

        if (User::where('email', $email)->doesntExist()) {
            return response([
                'message' => 'User doesnt exist'
            ], status: 404);
        }

        try {
            $token = \Str::random(20);
            $resetPassword = new ResetPassword();
            $resetPassword->token = $token;
            $resetPassword->email = $email;
            $resetPassword->save();

            Mail::to($email)->send(new PasswordSent($token));

            return response()->json([
                'message' => 'Check your email'
            ], 200);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 400);
        }
    }

    // Set new password
    public function newPassword(ValidateResetDataRequest $request)
    {
        $request->validated('token');
        $newPassword = \Hash::make($request->password);
        $email = $request->email;

        $currentTime = Carbon::now()->timestamp;
        $tokenCreatedAt = strtotime(ResetPassword::where('email', $email)->value('created_at'));

        //Set the time of token life
        if (($currentTime - $tokenCreatedAt) > 7200) {
            return response([
                'message' => 'You must generate new token'
            ]);
        } // Delete entity from DB
        elseif ($this->updatePasswordService->updateUserPassword($email, $newPassword)) {
            ResetPassword::where('email', $email)->delete();
        }
        return response()->json(null, 200);
    }

    // Updating User info
    public function update(UpdateUserRequest $updateUserRequest, User $user)
    {
        if ($updateUserRequest->user()->cannot('update', $updateUserRequest->user())) {
            return response()->json(null, 403);
        }

        $this->userService->updateUser($updateUserRequest->all(), $user);
        return response()->json(null, 200);
    }

    // Get list of all users
    public function showUsers(GetUsersRequest $getUsersRequest)
    {
        return response()->json(['users' => [$this->userService->getUsers()]], 200);
    }

    // Get info about current User
    public function showUser(GetUsersRequest $getUsersRequest, User $user)
    {
        if ($getUsersRequest->user()->cannot('update', $user)) {
            return response()->json(null, 403);
        }
        return response()->json(['userInfo' => $user], 200);
    }

    // Delete User & send confirmation via email
    public function delete(DeleteUsersRequest $deleteUsersRequest, User $user)
    {
        if ($deleteUsersRequest->user()->cannot('update', $user)) {
            return response()->json(null, 403);
        }
        $this->userService->deleteUser($user);

        $data['email'] = $user->email;
        $data['name'] = $user->name;
        $pdf = PDF\Pdf::loadView('emails.delete-user', $data);
        Mail::send('emails.delete-user', $data, function ($message) use ($data, $pdf) {
            $message->to($data['email'], $data['email'])
                ->subject('delete')
                ->attachData($pdf->output(), "delete.pdf");
        });

        return response()->json(null, 200);
    }
}
