<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateResetDataRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'token' => 'required|exists:reset_passwords',
            'email' => 'required|exists:reset_passwords',
            'password' => 'required|confirmed'
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
