<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetUsersRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => 'exists:users, id',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
