<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeleteUsersRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => 'exists:users, id',
            'email' => 'exists:users, email',
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
