<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class PasswordSent extends Mailable
{
    use Queueable, SerializesModels;

public $data;

    /**
     * @param $data
     */
    public function  __construct($data)
    {
$this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('resetpasswords@example.com')
            ->markdown('emails.passwords.password_sent');
    }
}
