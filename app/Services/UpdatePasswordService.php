<?php

namespace App\Services;

use App\Models\User;


class UpdatePasswordService
{
    public function updateUserPassword($email, $password)
    {
        return User::where('email', $email)->update(['password' => $password]);
    }
}
