<?php

namespace App\Services;

use App\Models\User;


class  UserService
{
    /**
     * Creating new user
     *
     * @param $data
     * @return User
     */
    public function createUser($data): User
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => \Hash::make($data['password']),
        ]);
    }

    public function updateUser($data, User $user)
    {
        return $user->fill($data)->save();
    }

    public function getUsers(): array
    {
        return User::all()->collect()->pluck('email')->all();
    }

    public function deleteUser(User $user)
    {
        return $user->whereId($user->id)->update(['status' => User::INACTIVE]);
    }

}



