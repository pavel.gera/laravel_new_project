<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;


/**
 * @mixin EloquentBuilder
 */
class ResetPassword extends Model
{
    protected $table = 'reset_passwords';
}
