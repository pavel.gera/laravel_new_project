@component('mail::message')
    # Reset password

    Your password have been reseted
    Token {{$data}}

    @component('mail::button', ['url' => 'http://localhost/api/newpassword/'])
        Reset with token
    @endcomponent

    Thanks,
    {{ config('app.name') }}
@endcomponent
